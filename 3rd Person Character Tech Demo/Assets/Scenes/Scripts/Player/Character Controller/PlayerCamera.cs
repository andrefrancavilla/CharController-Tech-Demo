using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCamera : CameraControls<PlayerCamera>
{
    /*
     * Camera behaviour:
     *   - Follow velocity depends on how far the player is from the camera
	 *       - Small movements will feel much more natural
	 *       - AnimationCurve to regulate velocity
     *   - Moves closer up and to the side of the player when standing still
     *   - Moves farther away when the player is moving, remaining on the player's right hand corner
     *   - When camera is too close to a wall, it slowly moves up or down (targetToLookAt rotates) to have a clear view 
     *   - Dynamic camera bob, sync with feet
     *
     *   Inherited Controls for PlayerCamera:
     *   - Camera Shake
     *
     */
    [Header("Camera Follow")]
    [SerializeField]private Vector3 positionOffset;
    [SerializeField]private Vector3 lookAtOffset;
    [SerializeField, Range(0, 1.5f)] private float cameraFollowWeight;
    [SerializeField]private LayerMask cameraRayMask;
    [SerializeField]private float offsetFromSurfaces;

    private RaycastHit camRay;
    [Header("Mouse Rotation")]
    [SerializeField]private float mouseXSpeed;
    [SerializeField]private float mouseYSpeed;
    [SerializeField]private float mouseSmoothAmount;
    [SerializeField]private float maxPitch;
    [SerializeField]private float minPitch;

    private Vector2 currMouseRotation;
    private Vector2 smoothMouseRotation;
    private Vector2 smoothRotationVel;

    private Transform targetToFollow;
    [NonSerialized] public Transform targetFollower;
    private Transform targetToLookAt;
    private Vector3 smoothFollowPosition;
    private Vector3 smoothFollowVel;

    // Start is called before the first frame update
    void Start()
    {
        SetTarget(GameObject.FindWithTag("Player").transform);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void FixedUpdate()
    {
        //Target Follower
        smoothFollowPosition = Vector3.SmoothDamp(smoothFollowPosition, targetToFollow.position, ref smoothFollowVel, cameraFollowWeight);
        targetFollower.position = smoothFollowPosition;
        targetToLookAt.localPosition = lookAtOffset;

        if (Physics.Raycast(targetToLookAt.position, -targetToLookAt.forward, out camRay, Mathf.Abs(positionOffset.z) + offsetFromSurfaces, cameraRayMask))
        {
            //Position camera on surface with offset to avoid clipping
            transform.position = Vector3.Lerp(transform.position, camRay.point + camRay.normal * offsetFromSurfaces, cameraFollowWeight);
        }
        else
        {
            //Let camera float
            transform.position = Vector3.Lerp(transform.position, targetToLookAt.TransformPoint(positionOffset), cameraFollowWeight);
        }
        
        transform.LookAt(targetToLookAt);

        //Camera Rotation
        currMouseRotation += new Vector2(Input.GetAxis("Mouse X") * mouseXSpeed, -Input.GetAxis("Mouse Y") * mouseYSpeed);
        currMouseRotation = new Vector2(currMouseRotation.x, Mathf.Clamp(currMouseRotation.y, minPitch, maxPitch));
        smoothMouseRotation = Vector2.SmoothDamp(smoothMouseRotation, currMouseRotation, ref smoothRotationVel, mouseSmoothAmount);
        
        targetFollower.rotation = Quaternion.Euler(new Vector3(0, smoothMouseRotation.x, 0));
        
        targetToLookAt.localRotation = Quaternion.Euler(new Vector3(smoothMouseRotation.y, 0, 0));
    }

    public void SetTarget(Transform target)
    {
        targetToFollow = target;
        smoothFollowPosition = target.position;
        
        targetFollower ??= new GameObject().transform;
        targetToLookAt ??= new GameObject().transform;
        targetToLookAt.parent ??= targetFollower;
    }
}
