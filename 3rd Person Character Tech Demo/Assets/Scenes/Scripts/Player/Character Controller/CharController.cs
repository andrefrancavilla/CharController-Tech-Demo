using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharController : Singleton<CharController>
{
    /*
     * The CharController controls ONLY the player capsule.
     * Animations and Player Movement are NOT TIED TOGETHER if not by input and input only.
     * The player's model is SEPARATE from the CharController and it's (possible) underlying GameObjects
     */
    [Header("Basic Movement Configuration")]
    [SerializeField] private float moveSpeed;
    [SerializeField] private float smoothMoveDelta = 0.075f;
    [SerializeField] private float movementDeadZone = 0.075f;
    [SerializeField] private float maxSlopeAmount;
    [SerializeField, Tooltip("In meters")] private float jumpHeight;
    //Basic internal
    private Rigidbody rb;
    private Vector2 localMoveVel;
    private Vector2 smoothMoveVel;
    private Vector3 globalMoveVel;

    [Header("Grounded Control")] 
    [Tooltip("All layers selected will be considered in the grounded"), SerializeField] private LayerMask groundDetectionFilter;
    private RaycastHit slopeDetector;
    private CapsuleCollider playerCollider;
    private bool jumped;
    
    /// <summary>
    /// the Jump height in meters converted into velocity
    /// </summary>
    private float jumpVelocity;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        playerCollider = GetComponent<CapsuleCollider>();
        jumpVelocity = Mathf.Sqrt(2 * jumpHeight * -Physics.gravity.y);
    }

    private void Update()
    {
        if (!jumped)
        {
            if (Input.GetButtonDown("Jump"))
            {
                Jump();
            }
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        localMoveVel = Vector2.ClampMagnitude(new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical")), 1);
        smoothMoveVel = Vector2.Lerp(smoothMoveVel, localMoveVel, smoothMoveDelta);
        if (Mathf.Abs(smoothMoveVel.magnitude) > movementDeadZone)
        {
            localMoveVel *= moveSpeed;
            smoothMoveVel = Vector2.Lerp(smoothMoveVel, localMoveVel, smoothMoveDelta);
            transform.rotation = Quaternion.LookRotation(new Vector3(rb.velocity.x, 0, rb.velocity.z), transform.up);
        }
        else
        {
            smoothMoveVel = Vector3.zero;
        }

        globalMoveVel = PlayerCamera.Instance.targetFollower.TransformDirection(new Vector3(smoothMoveVel.x, 0, smoothMoveVel.y));
        
        if (!jumped)
        {
            Ray ray = new Ray(transform.position, Vector3.down);
            if (Physics.SphereCast(ray, playerCollider.bounds.extents.x + 0.25f, out slopeDetector, playerCollider.bounds.extents.y + 0.25f, groundDetectionFilter))
            {
                var cross1 = Vector3.Cross(slopeDetector.normal, PlayerCamera.Instance.targetFollower.TransformDirection(new Vector3(smoothMoveVel.x, 0, smoothMoveVel.y)));
                var cross2 = Vector3.Cross(cross1, slopeDetector.normal);
                rb.velocity = cross2;
            }
            else
            {
                rb.velocity = new Vector3(globalMoveVel.x, rb.velocity.y, globalMoveVel.z);
            }
        }
        else
        {
            rb.velocity = new Vector3(globalMoveVel.x, rb.velocity.y, globalMoveVel.z);
        }
    }

    private void Jump()
    {
        jumped = true;
        rb.velocity = new Vector3(rb.velocity.x, jumpVelocity, rb.velocity.z);
    }

    private void OnCollisionEnter(Collision other)
    {
        for (int i = 0; i < other.contactCount; i++)
        {
            if (Vector3.Angle(other.GetContact(i).normal, Vector3.up) < maxSlopeAmount)
            {
                jumped = false;
            }
        }
    }
}
